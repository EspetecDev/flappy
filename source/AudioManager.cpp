#include "AudioManager.h"

AudioManager* AudioManager::s_pInstance;

AudioManager::AudioManager() {
	// Valores del libro supongo que bitrate y mierdas varias
	Mix_OpenAudio(22050, AUDIO_S16, 2, 4096);
}

bool AudioManager::load(std::string filename, std::string id, sound_type type) {
	if (type == MUSIC) {

		Mix_Music* pMusic = Mix_LoadMUS(filename.c_str());

		if (pMusic == 0) {
			std::cout << "[AudioManager::load] Could not load music file -> Error: " << Mix_GetError() << std::endl;
			return false;
		}
		m_musicList[id] = pMusic;
		return true;
	}
	else if (type == SFX) {

		Mix_Chunk* pChunk = Mix_LoadWAV(filename.c_str());

		if (pChunk == 0) {
			std::cout << "[AudioManager::load] Could not load sfx file -> Error: " << Mix_GetError() << std::endl;
			return false;
		}
		m_sfxList[id] = pChunk;
		return true;
	}
	return false;
}

bool AudioManager::isMusicPlaying()
{
	return Mix_PlayingMusic();
}

void AudioManager::playMusic(std::string id, int loop) {
	Mix_PlayMusic(m_musicList[id], loop);
}

void AudioManager::playSound(std::string id, int loop) {
	// -1 refers to the available channel
	Mix_PlayChannel(-1, m_sfxList[id], loop);
}

void AudioManager::stopMusic() {
	Mix_HaltMusic();
}

void AudioManager::setMusicVolume(int volume) {
	if (volume < 100 && volume >= 0)
		Mix_VolumeMusic(volume);
	else
		std::cout << "[AudioManager::setMusicVolume] Wrong volume value, must be between 0 - 100" << std::endl;
}

void AudioManager::setSfxVolume(std::string sound, int volume) {
	if (volume < 100 && volume >= 0)
		Mix_VolumeChunk(m_sfxList[sound], volume);
	else
		std::cout << "[AudioManager::setSfxVolume] Wrong volume value, must be between 0 - 100" << std::endl;
}

std::map<std::string, Mix_Chunk*>* AudioManager::getSfx()
{
	return &m_sfxList;
}

void AudioManager::clearFromAudioMap(std::string id, sound_type type) {
	if (type = MUSIC) {
		itM = m_musicList.find(id);
		if (itM != m_musicList.end())
		{
			Mix_FreeMusic(itM->second);
			itM->second = NULL;
			m_musicList.erase(id);
		}
	}

	else if (type = SFX) {
		itS = m_sfxList.find(id);
		if (itS != m_sfxList.end())
		{
			Mix_FreeChunk(itS->second);
			itS->second = NULL;
			m_sfxList.erase(id);
		}
	}
}