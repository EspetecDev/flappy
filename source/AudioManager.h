#ifndef AUDIOMANAGER_H
#define AUDIOMANAGER_H
#include <string>
#include <map>
#include <iostream>
#include <SDL_mixer.h>

enum sound_type {
	MUSIC = 0,
	SFX = 1
};

class AudioManager {

public:
	
	static AudioManager* Instance() {
		if (s_pInstance == 0) {
			s_pInstance = new AudioManager();
			return s_pInstance;
		}
		return s_pInstance;
	}

	bool load(std::string filename, std::string id, sound_type type);
	bool isMusicPlaying();
	void playMusic(std::string id, int loop);
	void playSound(std::string id, int loop);
	void stopMusic();
	void setMusicVolume(int volume);
	void setSfxVolume(std::string sound, int volume);
	void clearFromAudioMap(std::string id, sound_type tpye);
	std::map<std::string, Mix_Chunk*>* getSfx();

private:

	static AudioManager* s_pInstance;

	std::map<std::string, Mix_Chunk*> m_sfxList;
	std::map<std::string, Mix_Music*> m_musicList;
	std::map<std::string, Mix_Chunk*>::iterator itS;
	std::map<std::string, Mix_Music*>::iterator itM;

	AudioManager();
	~AudioManager() { Mix_CloseAudio(); };

	AudioManager(const AudioManager&);
	AudioManager &operator=(const AudioManager&);
};

typedef AudioManager audioManager;
#endif