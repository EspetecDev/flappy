#include "GameState.h"
#include <vector>

class GameStateMachine {

public:

	GameStateMachine();
	void pushState(GameState* pState);
	void changeState(GameState* pState);
	void popState();
	void update();
	void render();
	void setCurrentScore(unsigned long cs) { currentScore = cs; }
	void setHighestScore(unsigned long hs) { highestScore = hs; }
	unsigned int long getCurrentScore() { return currentScore; }
	unsigned int long getHighestScore() { return highestScore; }

private:
	unsigned long currentScore;
	unsigned long highestScore;
	std::vector<GameState*> m_gameStates;
};