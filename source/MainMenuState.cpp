#include "MainMenuState.h"
#include "TextureManager.h"
#include "Game.h"
#include "MenuButton.h"
#include "PlayState.h"
#include "AudioManager.h"

const std::string MainMenuState::s_menuID = "MENU";

void MainMenuState::update(){
	if (!m_gameObjects.empty()) {
		for (GameObject* go : m_gameObjects) {
			if (!m_gameObjects.size())
				break;
			go->update();
		}
	}
	if (!m_uiTextVector.empty()) {
		for (UIText* ui : m_uiTextVector) {
			if (!m_uiTextVector.size())
				break;
			ui->update();
		}
	}
}

void MainMenuState::render(){
	if (!m_gameObjects.empty()) {
		for (GameObject* go : m_gameObjects)
			go->draw();
	}
	if (!m_uiTextVector.empty()) {
		for (UIText* ui : m_uiTextVector)
			ui->draw();
	}
}

bool MainMenuState::onEnter(){
	// OnEnter loads the files for the state we are going to be
	std::cout << "[MainMenuState::onEnter] Entering MainMenuState " << std::endl;
	unsigned long highestScore = game::Instance()->getStateMachine()->getHighestScore();
	std::cout << "[MainMenuState::onEnter] highest score:  " << highestScore << std::endl;
	// UI Buttons

	if (!textureManager::Instance()->load("assets/playButton.png", "playbutton", game::Instance()->getRenderer()))
		return false;
	if (!textureManager::Instance()->load("assets/exitButton.png", "exitbutton", game::Instance()->getRenderer()))
		return false;

	if (!textureManager::Instance()->load("assets/menuBackground.png", "menubackground", game::Instance()->getRenderer()))
		return false;

	GameObject* background = new SDLGameObject(new LoaderParams(0, 0, 360 * SCALING, 640 * SCALING, "menubackground"));
	m_gameObjects.push_back(background);

	GameObject* button1 = new MenuButton(new LoaderParams((SCREEN_WIDTH / 2) - 65, (SCREEN_HEIGHT /2) -30, 130, 60, "playbutton"), s_menuToPlayState); //169,379
	GameObject* button2 = new MenuButton(new LoaderParams((SCREEN_WIDTH / 2) - 52.5, ((SCREEN_HEIGHT / 2) - 30)+75, 105, 60, "exitbutton"), s_exitFromMenu); //218,506

	m_gameObjects.push_back(button1);
	m_gameObjects.push_back(button2);

	// UI Text
	TTF_Init();
	TTF_Font* font;
	font = TTF_OpenFont("assets/Fonts/FFFFORWA.ttf", 12);

	UIText* uiText1 = new UIText(new LoaderParams((SCREEN_WIDTH / 2) - 82, ((SCREEN_HEIGHT / 2) - 15) + 155, 141, 30, "notUsed"), game::Instance()->getRenderer()); //145, 632

	uiText1->load("Highest Score : " + std::to_string(highestScore), TEXT_COLORS.find("black")->second, font, NULL);

	m_uiTextVector.push_back(uiText1);

	TTF_CloseFont(font);
	TTF_Quit();
	
	// You are listening to ThisGameRadio, and here it is Menu music
	// hmm Menu music, I like it
	audioManager::Instance()->load("assets/music/menuMusic.mp3", "menuMusic", MUSIC);
	audioManager::Instance()->setMusicVolume(30);
	audioManager::Instance()->playMusic("menuMusic", -1);
	return true;
}

bool MainMenuState::onExit(){
	std::cout << "[MainMenuState::onExit] Exiting MainMenuState " << std::endl;
	for (GameObject* go : m_gameObjects) {
		go->clean();
		delete go;
	}
	m_gameObjects.clear();
	m_gameObjects.shrink_to_fit();

	for (UIText* ui : m_uiTextVector) {
		ui->clean();
		delete ui;
	}
	m_uiTextVector.clear();
	m_uiTextVector.shrink_to_fit();
	textureManager::Instance()->clearFromTextureMap("playbutton");
	textureManager::Instance()->clearFromTextureMap("exitbutton");
	textureManager::Instance()->clearFromTextureMap("menubackground");
	audioManager::Instance()->clearFromAudioMap("menuMusic", MUSIC);

	return true;
}

void MainMenuState::s_menuToPlayState(){
	std::cout << "Play button clicked!" << std::endl;
	game::Instance()->getStateMachine()->changeState(new PlayState());
}

void MainMenuState::s_exitFromMenu(){
	std::cout << "Exit button clicked!" << std::endl;
	game::Instance()->quit();
}
