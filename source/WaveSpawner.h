#ifndef WAVESPAWNER_H
#define WAVESPAWNER_H
#include "Vector2D.h"
#include <vector>

class WaveSpawner {


public:
	
	WaveSpawner(float enemyMultiplyer, int firstWave);

	void spawnWaves();
	void setPosition(Vector2D pos) { m_position = pos; }
	Vector2D getPosition() { return m_position; }
	int getEnemyCount(){ return m_waves[m_currentWave]; }
	int getCurrentWave() { return m_currentWave; }
	std::vector<int> getWavesArray() { return m_waves; }
	void addWave(int numEnemies) { m_waves.push_back(numEnemies); }
private:

	Vector2D m_position = Vector2D(0,0);
	int m_waveSize;
	float m_spawnRate = 0.5f;
	float m_enemyMultiplyer;
	unsigned int m_lastTime = 0;
	unsigned int m_currentTime;
	unsigned int m_nextWaveTime;
	unsigned int m_currentWave = 0;
	std::vector<int> m_waves;
	void createEvent();

};

#endif // !WAVESPAWNER_H
