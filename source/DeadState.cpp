#include <fstream>
#include "DeadState.h"
#include "Game.h"
#include "TextureManager.h"
#include "MenuButton.h"
#include "MainMenuState.h"
#include "PlayState.h"
#include "AudioManager.h"


const std::string DeadState::s_menuID = "DEAD";

void DeadState::update(){
	// Let's make a transition animation
	if ((SDL_GetTicks() / 1000) > 2)
		transitionComplete = true;

	if (transitionComplete) {
		for (GameObject* go : m_gameObjects) {
			if (!m_gameObjects.size())
				break;
			go->update();
		}
		
		for (UIText* ui : m_uiTextVector) {
			if (!m_uiTextVector.size())
				break;
			ui->update();
		}
	}
}

void DeadState::render(){
	if (transitionComplete) {
		for (GameObject* go : m_gameObjects)
			go->draw();
		
		for (UIText* ui : m_uiTextVector)
			ui->draw();
	}
}

bool DeadState::onEnter(){
	// OnEnter loads the files for the state we are going to be
	std::cout << "[DeadState::onEnter] Entering DeadState " << std::endl;

	// Check if currentScore is a new record
	unsigned long currentScore = game::Instance()->getStateMachine()->getCurrentScore();
	unsigned long highestScore = game::Instance()->getStateMachine()->getHighestScore();
	std::string currentRecord = std::to_string(highestScore);
	if (game::Instance()->getStateMachine()->getCurrentScore() > highestScore) {
		game::Instance()->getStateMachine()->setHighestScore(game::Instance()->getStateMachine()->getCurrentScore());
		highestScore = game::Instance()->getStateMachine()->getHighestScore();
		currentRecord = std::to_string(highestScore) + " NEW RECORD!!";

		// Save highestScore
		std::ofstream saveFile("save.sav", std::ios::binary | std::ios::out);
		if (saveFile.is_open()) {
			saveFile.write((char*)&highestScore, sizeof(unsigned long));
			saveFile.close();
		}
		else {
			std::cout << "[GameStateMachine::GameStateMachine] Unable to save the highest score" << std::endl;
		}

	}

	if (!textureManager::Instance()->load("assets/restartButton.png", "restartbutton", game::Instance()->getRenderer()))
		return false;
	if (!textureManager::Instance()->load("assets/mainMenuButton.png", "mainbutton", game::Instance()->getRenderer()))
		return false;

	if (!textureManager::Instance()->load("assets/gameOverBackground.png", "gameoverbackground", game::Instance()->getRenderer()))
		return false;

	GameObject* background = new SDLGameObject(new LoaderParams(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, "gameoverbackground"));
	m_gameObjects.push_back(background);

	GameObject* button1 = new MenuButton(new LoaderParams((SCREEN_WIDTH / 2) - 81, (SCREEN_HEIGHT / 2) - 37.5, 162, 75, "restartbutton"), s_playAgain);
	GameObject* button2 = new MenuButton(new LoaderParams((SCREEN_WIDTH / 2) - 65, ((SCREEN_HEIGHT / 2) - 30) + 90, 130, 60, "mainbutton"), s_exitToMenu);

	m_gameObjects.push_back(button1);
	m_gameObjects.push_back(button2);

	// UI text
	TTF_Init();
	TTF_Font* font = TTF_OpenFont("assets/Fonts/FFFFORWA.ttf", 20);

	UIText* uiTextScore = new UIText(new LoaderParams((SCREEN_WIDTH / 2) - 75, (SCREEN_HEIGHT / 4) + 39, 214, 35, "notUsed"), game::Instance()->getRenderer()); 
	uiTextScore->load("Score : " + std::to_string(currentScore), TEXT_COLORS.find("black")->second, font, NULL);
	m_uiTextVector.push_back(uiTextScore);

	TTF_Font* font2 = TTF_OpenFont("assets/Fonts/FFFFORWA.ttf", 12);

	UIText* uiTextHighScore = new UIText(new LoaderParams((SCREEN_WIDTH / 2) - 110, 2*(SCREEN_HEIGHT/3) + 35, 251, 30, "notUsed"), game::Instance()->getRenderer());
	uiTextHighScore->load("Current record : " + currentRecord, TEXT_COLORS.find("black")->second, font2, NULL);
	m_uiTextVector.push_back(uiTextHighScore);

	initTimer = SDL_GetTicks();

	TTF_CloseFont(font);
	TTF_CloseFont(font2);

	TTF_Quit();

	return true;
}

bool DeadState::onExit(){
	std::cout << "[MainMenuState::onExit] Exiting MainMenuState " << std::endl;
	for (GameObject* go : m_gameObjects) {
		go->clean();
		delete go;
	}
	m_gameObjects.clear();
	m_gameObjects.shrink_to_fit();
	
	for (UIText* ui : m_uiTextVector) {
		ui->clean();
		delete ui;
	}
	m_uiTextVector.clear();
	m_uiTextVector.shrink_to_fit();
	textureManager::Instance()->clearFromTextureMap("restartbutton");
	textureManager::Instance()->clearFromTextureMap("exitbutton");
	textureManager::Instance()->clearFromTextureMap("gameoverbackground");

	//Sonido cargado en PlayState pero que persiste en DeadState
	audioManager::Instance()->clearFromAudioMap("tankDeath", SFX);
	return true;
}

void DeadState::s_playAgain(){
	std::cout << "Restart button clicked!" << std::endl;
	game::Instance()->getStateMachine()->changeState(new PlayState());
}

void DeadState::s_exitToMenu() {
	std::cout << "Exit button clicked!" << std::endl;
	game::Instance()->getStateMachine()->changeState(new MainMenuState());
}
