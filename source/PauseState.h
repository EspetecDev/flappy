#ifndef PAUSESTATE_H
#define PAUSESTATE_H
#include "GameObject.h"
#include "GameState.h"
#include <iostream>
#include <vector>
#include "UIText.h"
#include "Constants.h"

class PauseState : public GameState {

public:

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_pauseID; }

private:

	static void s_pauseToMain();
	static void s_resumePlay();

	static const std::string s_pauseID;

	std::vector<GameObject*> m_gameObjects;
	std::vector<UIText*> m_uiTextVector;
};

#endif