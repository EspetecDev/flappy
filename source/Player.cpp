#include "Player.h"
#include "InputHandler.h"
#include "AudioManager.h"

Player::Player(const LoaderParams* pParams) : SDLGameObject(pParams) {
	m_collider = (new Collider(this, new Vector2D(10, 45), new Vector2D(75, 50)));

}

/* Drawing Player */
void Player::draw() {
	SDLGameObject::draw();
}

void Player::update() {
	if (m_lives <= 0 && !m_dying)
		destroy();

	if (!m_dying) // If it's alive
	{
		//Movement
		m_velocity += m_acceleration;
		m_position += m_velocity;
		if (m_position.getX() < -49)m_position.setX(-49);
		if (m_position.getX() > SCREEN_WIDTH - 65)m_position.setX(SCREEN_WIDTH - 65);
		m_velocity.setX(0.0);

		//Input
		m_coolDown--;
		handleInput();

		//Play animations
		if (m_textureID == "player-shooting" && m_currentFrame < ANIMATION_PLAYER_SHOOTING_FRAMES - 1) {
			if (SDL_GetTicks() - m_animationTimer > 50) {
				m_currentFrame++;
				m_animationTimer = SDL_GetTicks();
			}
		}
		else {
			if (m_textureID != "player-dead")
				m_textureID = "player-still";
			m_currentFrame = int((SDL_GetTicks() / 100) % 1);
		}
	}
	else
	{
		//Play animation
		if (SDL_GetTicks() - m_animationTimer > 50) {
			m_currentFrame++;
			m_animationTimer = SDL_GetTicks();
		}

		// Check if animation has been played entirely
		if (m_currentFrame > ANIMATION_PLAYER_DYING_FRAMES - 1)
		{
			m_deadFrames--;
			if (m_deadFrames > 0)
				m_currentFrame--;
			else
			{
				SDL_Event event;
				SDL_memset(&event, 0, sizeof(event));
				event.type = deadStateEvent;
				SDL_PushEvent(&event);
			}
		}
	}
}

void Player::clean() {
	SDLGameObject::clean();
}


void Player::shoot() {
	if (m_coolDown < 0) {
		m_coolDown = PLAYER_FRAMES_BETWEEN_BULLETS;

		// Play Animation
		m_textureID = "player-shooting";
		m_currentFrame = 0;
		m_animationTimer = SDL_GetTicks();

		// Play sfx
		audioManager::Instance()->playSound("tankShot", 0);

		// Create Bullet
		SDL_Event event;
		SDL_memset(&event, 0, sizeof(event));
		event.type = createEntityEvent;

		event.user.data1 = new Bullet(new LoaderParams((int)(m_position.getX() + 49), (int)(m_position.getY() + 5), 16, 16, "player-bullet"),
			new Vector2D(0.0f, -10.0f), new Vector2D(0.0f, 0.0f), true);
		SDL_PushEvent(&event);
	}
}

void Player::hit(int damage) {
	m_lives -= damage;
}

void Player::handleInput() {
	// Movement
	if (inputHandler::Instance()->isKeyDown(SDL_SCANCODE_A))
		m_velocity.setX(-4.0);
	if (inputHandler::Instance()->isKeyDown(SDL_SCANCODE_D)) 
		m_velocity.setX(4.0);

	// Shooting
	if (inputHandler::Instance()->isKeyDown(SDL_SCANCODE_L) || inputHandler::Instance()->isKeyDown(SDL_SCANCODE_SPACE))
		shoot();
}

void Player::destroy()
{
	//Change state
	m_dying = true;

	//Play animation
	m_textureID = "player-dying";
	m_currentFrame = 0;
	m_animationTimer = SDL_GetTicks();

	//Play sfx
	AudioManager::Instance()->playSound("tankDeath", 0);
}