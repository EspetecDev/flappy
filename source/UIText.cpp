#include "UIText.h"

UIText::UIText(const LoaderParams* pParams, SDL_Renderer* renderer) : GameObject(pParams), m_position((float)pParams->getX(), (float)pParams->getY()), m_renderer(renderer) {
}

void UIText::load(std::string textureText, SDL_Color color, TTF_Font* font, SDL_Renderer* renderer) {
	SDL_Surface* text = TTF_RenderText_Solid(font, textureText.c_str(), color);
	if (!text) {
		std::cout << "Failed to render text" << std::endl;
	}
	else {
		m_texture = SDL_CreateTextureFromSurface((renderer == NULL) ? m_renderer : renderer, text);
		if (!m_texture) {
			std::cout << "Texture could not be created from surface text" << std::endl;
		}
		else {
			m_height = text->h;
			m_width = text->w;
		}
	}
	SDL_FreeSurface(text);

	//Saving the color and font used
	m_color = color;
	m_font = font;
}

void UIText::drawUpdate(Vector2D vector, SDL_Renderer* renderer) {
	// Specifies the coordinates for SDL_RenderCopy
	SDL_Rect renderQuad = { (int)vector.getX(), (int)vector.getY(), m_width, m_height };

	// Render to screen
	SDL_RenderCopyEx(renderer, m_texture, NULL, &renderQuad, 0, NULL, SDL_FLIP_NONE);
}

void UIText::updateValues(std::string newTextureText, SDL_Renderer* newRenderer = NULL, SDL_Color* newColor = NULL, TTF_Font* newFont = NULL) {
	if (m_texture != NULL)
		SDL_DestroyTexture(m_texture);
	load(newTextureText, (newColor == NULL) ? m_color : *newColor, (newFont == NULL) ? m_font : newFont, (newRenderer == NULL) ? m_renderer : newRenderer);
}

void UIText::draw()
{
	drawUpdate(m_position, m_renderer);
}

void UIText::update()
{
	//Update text values maybe?
	/* Example updating values (it generates a new texture each time)
	updateValues("uiTextTest" + std::to_string(m_counter), m_renderer, NULL, NULL);
	m_counter++;
	if (m_counter > 8999)
		m_counter = 0;		//It's not over 9000!
	*/


}

void UIText::clean() {
	SDL_DestroyTexture(m_texture);
	m_texture = NULL;
	m_renderer = NULL;
	m_font = NULL;
	m_text.clear();
}

