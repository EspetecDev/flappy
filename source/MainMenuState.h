#ifndef MAINMENUSTATE_H
#define MAINMENUSTATE_H
#include "GameState.h"
#include "GameObject.h"
#include "UIText.h"
#include <iostream>
#include <vector>

class MainMenuState : public GameState {

public:

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_menuID; }

private:

	std::string highestScore = "0548468";
	static const std::string s_menuID;
	std::vector<GameObject*> m_gameObjects;
	std::vector<UIText*> m_uiTextVector;

	// Callback functions for menu items
	static void s_menuToPlayState();
	static void s_exitFromMenu();
};
#endif