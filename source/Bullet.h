#ifndef BULLET_H
#define BULLET_H

#include "SDLGameObject.h"

class Bullet : public SDLGameObject{
public:

	Bullet (const LoaderParams* pParams, Vector2D* velocity, Vector2D* acceleration, bool ally);

	void Bullet::collidedWith(SDLGameObject* other);
	virtual void draw();
	virtual void update();
	virtual void clean();

	bool isAlly() {	return m_ally; }

private:

	void destroy();
	bool m_ally;
};

#endif