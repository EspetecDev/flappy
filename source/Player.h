#ifndef PLAYER_H
#define PLAYER_H

#include "SDLGameObject.h"
#include <iostream>
#include "Bullet.h"

class Player : public SDLGameObject {

public:

	Player(const LoaderParams* pParams);

	virtual void draw();
	virtual void update();
	virtual void clean();
	void hit(int damage);

	virtual int getLives() { if (m_lives < 0) return 0; else return m_lives; }

private:

	void shoot();
	void handleInput();
	void destroy();

	int m_animationTimer;
	int m_coolDown = 0;
	int m_lives = 3;
	bool m_dying = false;
	int m_deadFrames = ANIMATION_ENEMY_DEAD_FRAMES;
};
#endif