#ifndef SDLGAMEOBJECT_H
#define SDLGAMEOBJECT_H

#include "TextureManager.h"
#include "GameObject.h"
#include "Vector2D.h"
#include <string> 
#include "Collider.h"
#include "Constants.h"

class SDLGameObject : public GameObject {

public:

	SDLGameObject(const LoaderParams* pParams);

	virtual void draw();
	virtual void update();
	virtual void clean();
	virtual void collidedWith(SDLGameObject* other);

	int getWidth() {return m_width;}
	int getHeight() {return m_height;}
	Vector2D getPosition() {return m_position;}
	Vector2D* getCentre() { return new Vector2D(m_position.getX() + m_width/2.0f, m_position.getY() + m_height/2.0f); }

protected:

	Collider* m_collider;
	Vector2D m_position;
	Vector2D m_velocity;
	Vector2D m_acceleration;

	int m_width;
	int m_height;

	int m_currentRow;
	int m_currentFrame;

	std::string m_textureID;
};
#endif