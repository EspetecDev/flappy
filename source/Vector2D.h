#ifndef VECTOR2D_H
#define VECTOR2D_H

#include <math.h>

class Vector2D {

public:

	Vector2D(float x, float y) : m_x(x), m_y(y) {}

	float getX() { return m_x; }
	float getY() { return m_y; }

	void setX(float x) { m_x = x; }
	void setY(float y) { m_y = y; }

	// Returns length of the vector
	float length() { return (float)sqrt(m_x * m_x + m_y * m_y); }
	// Adds to vector and returns the result
	Vector2D operator+(const Vector2D& v2) const {
		return Vector2D(m_x + v2.m_x, m_y + v2.m_y);
	}
	// Adds himself and Vector2D v2
	// Making it friend lets us to access private members from both vectors
	friend Vector2D& operator+=(Vector2D& v1, const Vector2D& v2) {
		v1.m_x += v2.m_x;
		v1.m_y += v2.m_y;

		return v1;
	}
	// Multiply the vector by a scalar value
	Vector2D operator*(float scalar) {
		return Vector2D(m_x * scalar, m_y * scalar);
	}
	// Multiplies himself by a scalar value
	Vector2D& operator*=(float scalar) {
		m_x *= scalar;
		m_y *= scalar;

		return *this;
	}
	// Substraction 
	Vector2D operator-(const Vector2D& v2) const {
		return Vector2D(m_x - v2.m_x, m_y - v2.m_y);
	}
	friend Vector2D& operator-=(Vector2D& v1, const Vector2D& v2) {
		v1.m_x -= v2.m_x;
		v1.m_y -= v2.m_y;
		
		return v1;
	}
	// Divide vector by a scalar value
	Vector2D operator/(float scalar) {
		return Vector2D(m_x / scalar, m_y / scalar);
	}
	Vector2D& operator/=(float scalar) {
		m_x /= scalar;
		m_y /= scalar;

		return *this;
	}
	// Normalizing a vector
	void normalize() {
		float l = length();
		if (l > 0)
			(*this) *= 1 / l;
	}


private:

	float m_x;
	float m_y;

};

#endif