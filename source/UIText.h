#ifndef UITEXT_H
#define UITEXT_H
#pragma once
#include "GameObject.h"
#include "SDL_TTF.h"
#include "Vector2D.h"

class UIText : public GameObject {

public:
	UIText(const LoaderParams* pParams, SDL_Renderer* renderer);

	virtual void load(std::string textureText, SDL_Color color, TTF_Font* font, SDL_Renderer* renderer);
	virtual void drawUpdate(Vector2D vector, SDL_Renderer* renderer);
	virtual void updateValues(std::string newText, SDL_Renderer* renderer, SDL_Color* color, TTF_Font* font);
	virtual void draw();
	virtual void update();
	virtual void clean();

private:

	int m_height, m_width;
	std::string m_text;

	SDL_Texture* m_texture;
	SDL_Renderer* m_renderer;

	Vector2D m_position;

	SDL_Color m_color;
	TTF_Font* m_font;

	int m_counter = 0;

};
#endif // UITEXT_H
