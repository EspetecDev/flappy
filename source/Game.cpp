#include "Game.h"
#include "MainMenuState.h"
#include "PlayState.h"

Game* Game::s_pInstance = 0;

Uint32 createEntityEvent;
Uint32 destroyEntityEvent;
Uint32 updateScoreEvent;
Uint32 deadStateEvent;

bool Game::init(const char *title, int xPos, int yPos, bool fullscreen){

	int flags = 0;
	if (fullscreen) flags = SDL_WINDOW_FULLSCREEN;

	// Trying to initialize SDL
	if (!SDL_Init(SDL_INIT_EVERYTHING)) {
		std::cout << "[GAME::Init] 1/3 - Init success!" << std::endl;

		//Detectar resolución del pc
		SDL_DisplayMode DM;
		SDL_GetCurrentDisplayMode(0, &DM);

		std::cout << "[main] PC's screen height is " << DM.h << std::endl;
		if (DM.h > 960) {
			SCALING = 1.5f;
			SCREEN_WIDTH = (int)(SCREEN_WIDTH*SCALING);
			SCREEN_HEIGHT = (int)(SCREEN_HEIGHT*SCALING);
		}

		// Window init
		m_pWindow = SDL_CreateWindow(title, xPos, yPos, SCREEN_WIDTH, SCREEN_HEIGHT, flags);
		if (m_pWindow != 0) {
			std::cout << "[GAME::Init] 2/3 - Window init success!" << std::endl;
			
			// Renderer init
			m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, 0);
			if (m_pRenderer != 0) {
				std::cout << "[GAME::Init] 3/3 - Renderer init success!" << std::endl;
				
				// Painting screen in white
				SDL_SetRenderDrawColor(m_pRenderer, 200, 0, 0, 255);
				SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");
			}
			else{
				std::cout << "[GAME::Init] 3/3 - Renderer init failed!" << std::endl;
				return false;
			}
		}
		else {
			std::cout << "[GAME::Init] 2/3 - Window init failed!" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "[GAME::Init] 1/3 - SDL init failed!" << std::endl;
		return false;
	}

	// Set icon window
	SDL_Surface* iconSurface = IMG_Load("assets/windowIcon.png");
	SDL_SetWindowIcon(m_pWindow, iconSurface);

	// Joystick initialization
	inputHandler::Instance()->initJoysticks();

	std::cout << "***** [GAME::Init] Initialitation successful ******" << std::endl;
	m_bRunning = true;

	std::cout << "[Game::Init] Creating states" << std::endl;
	m_pGameStateMachine = new GameStateMachine();
	m_pGameStateMachine->changeState(new MainMenuState());
	
	// Register events
	createEntityEvent = SDL_RegisterEvents(1);
	if (createEntityEvent == ((Uint32)-1))
		std::cout << "***** [GAME::Init] Failed registering create entity event!" << std::endl;

	destroyEntityEvent = SDL_RegisterEvents(1);
	if (destroyEntityEvent == ((Uint32)-1))
		std::cout << "***** [GAME::Init] Failed registering destroy entity event!" << std::endl;

	updateScoreEvent = SDL_RegisterEvents(1);
	if (updateScoreEvent == ((Uint32)-1))
		std::cout << "***** [GAME::Init] Failed registering update score event!" << std::endl;

	deadStateEvent = SDL_RegisterEvents(1);
	if (deadStateEvent == ((Uint32)-1))
		std::cout << "***** [GAME::Init] Failed registering update score event!" << std::endl;
	
	return true;
}

void Game::render(){

	// Clears the renderer to draw color
	SDL_RenderClear(m_pRenderer);
	// Render current state
	m_pGameStateMachine->render();
	// Draw to the screen
	SDL_RenderPresent(m_pRenderer);
}

void Game::update(){
	m_pGameStateMachine->update();
}

void Game::handleEvents(){
	//SDL_Event event;
	inputHandler::Instance()->update();
}

void Game::clean(){

	std::cout << "[GAME::Clear] Cleaning..." << std::endl;
	inputHandler::Instance()->clean();
	SDL_DestroyWindow(m_pWindow);
	SDL_DestroyRenderer(m_pRenderer);
	SDL_Quit();
}
