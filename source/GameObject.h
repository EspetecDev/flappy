#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "SDL.h"
#include <iostream>
#include "LoaderParams.h"

class GameObject {
public:
	
	// Functions are becoming pure virtual with =0
	virtual void draw() = 0;
	virtual void update() = 0;
	virtual void clean() = 0;
	virtual ~GameObject() {}
protected:

	GameObject(const LoaderParams* pParams){}
	//virtual ~GameObject() {}
};

#endif // !GAMEOBJECT_H