#ifndef GAME_H
#define GAME_H

#include "SDL.h"
#include "SDL_image.h"
#include <vector>
#include "GameObject.h"
#include <string>
#include <iostream>
#include "TextureManager.h"
#include "Player.h"
#include "Enemy.h"
#include "InputHandler.h"
#include "GameStateMachine.h"
#include "Constants.h"


class Game
{
public:
	
	bool init(const char *title, int xPos, int yPos, bool fullscreen);
	void render();
	void update();
	void handleEvents();
	void clean();
	void quit() { m_bRunning = false; }
	bool running() { return m_bRunning; }
	static Game* Instance() {
		if (!s_pInstance) {
			s_pInstance = new Game();
			return s_pInstance;
		}
		return s_pInstance;
	}
	SDL_Renderer* getRenderer() const { return m_pRenderer; }
	GameStateMachine* getStateMachine() { return m_pGameStateMachine; }

private:

	Game() {}
	~Game() {}

	static Game* s_pInstance;

	SDL_Window* m_pWindow; 
	SDL_Renderer* m_pRenderer;

	bool m_bRunning;

	int m_currentFrame;

	std::vector<GameObject*> m_gameObjects;
	int m_currentState;
	GameStateMachine* m_pGameStateMachine;
};
typedef Game game;

enum game_states {
	MENU = 0,
	PLAY = 1,
	GAMEOVER = 2
};
#endif