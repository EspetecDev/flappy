#ifndef ENEMY_H
#define ENEMY_H

#include "SDLGameObject.h"
#include "Bullet.h"
#include "Player.h"
#include "AudioManager.h"

class Enemy : public SDLGameObject{
public:

	Enemy(const LoaderParams* pParams, Vector2D* velocity, Vector2D* acceleration, int limit);

	virtual void draw();
	virtual void update();
	void hit(int damage);
	virtual void clean();

private:
	int m_hp = 30;
	void destroy();
	void shoot();
	int m_limit; //Limit where the enemy won't move
	int m_coolDown = ENEMY_INITIAL_COOLDOWN;
	int m_animationTimer;
	bool m_dying = false;
};

#endif