#include "GameStateMachine.h"
#include <iostream>
#include <fstream>
#include <bitset>

GameStateMachine::GameStateMachine() {
	// Load the highestScore from .sav
	// binary is for binary files, in for input (no fking way :O)
	unsigned long fileScore = 0;
	std::ifstream saveFile("save.sav", std::ios::binary | std::ios::in);
	if (saveFile.is_open()) {
		saveFile.read((char*)&fileScore, sizeof(unsigned long));	
		std::cout << "[GameStateMachine::GameStateMachine] File score value: "<< fileScore << std::endl;
		highestScore = fileScore;
		saveFile.close();
	}
	else {
		std::cout << "[GameStateMachine::GameStateMachine] Unable to load sav file, setting highestScore to 0" << std::endl;
		highestScore = 0;
	}
	
}

void GameStateMachine::pushState(GameState * pState){
	// We push the state into the vector and launch onEnter()
	m_gameStates.push_back(pState);
	m_gameStates.back()->onEnter();
}


void GameStateMachine::changeState(GameState * pState){
	
	if (!m_gameStates.empty()) {
		if (m_gameStates.back()->getStateID() == pState->getStateID())
			return; // do nothing
		if (m_gameStates.back()->onExit()) {
			m_gameStates.pop_back();
		}
	}

	// Init
	m_gameStates.push_back(pState);
	pState->onEnter();
	pState->update();
	// Push back new state
	std::cout << "[GameStateMachine::changeState] Number of states: " << m_gameStates.size() << std::endl;

}

void GameStateMachine::popState(){
	// Checking if there's any state to be deleted
	if(!m_gameStates.empty())
		if (m_gameStates.back()->onExit()) {
			delete m_gameStates.back();
			m_gameStates.pop_back();
		}
}

void GameStateMachine::update(){
	if (!m_gameStates.empty())
		m_gameStates.back()->update();
}

void GameStateMachine::render(){
	if (!m_gameStates.empty())
		m_gameStates.back()->render();
}
