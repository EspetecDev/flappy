#ifndef PLAYSTATE_H
#define PLAYSTATE_H
#include "GameObject.h"
#include "GameState.h"
#include <iostream>
#include <vector>
#include "UIText.h"
#include "WaveSpawner.h"

class PlayState : public GameState {

public:

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_playStateID; }

private:

	static const std::string s_playStateID;
	std::vector<GameObject*> m_gameObjects;
	std::vector<UIText*> m_uiTextVector;
	static void s_deadState();

	WaveSpawner *m_waveSpawner;
	unsigned int m_nextWaveTime;
	unsigned long m_lastWave;
	void handleEvents();
	int m_score = 0;
};

#endif