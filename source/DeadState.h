#ifndef DEADSTATE_H
#define DEADSTATE_H
#include "GameState.h"
#include "GameObject.h"
#include "UIText.h"
#include <iostream>
#include <vector>

class DeadState : public GameState {

public:

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_menuID; }

private:

	static const std::string s_menuID;
	std::vector<GameObject*> m_gameObjects;
	std::vector<UIText*> m_uiTextVector;
	bool transitionComplete = false;
	Uint32 initTimer;

	// Callback functions for menu items
	static void s_playAgain();
	static void s_exitToMenu();
};
#endif