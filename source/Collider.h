#ifndef COLLIDER_H
#define COLLIDER_H

#include "Vector2D.h"
class SDLGameObject;
struct SDL_Renderer;

class Collider 
{
public:
	Collider(SDLGameObject* gameObject);
	Collider(SDLGameObject* gameObject, Vector2D* origin, Vector2D* size);
	Vector2D getPosition() ;
	bool checkIntersection(Collider* collider);
	void draw();
	~Collider();
	SDLGameObject* getSDLGameObject() { return m_gameObject; }

private:
	bool rangeIntersect(float min1, float max1, float min2, float max2);
	SDLGameObject* m_gameObject;
	Vector2D m_positionOffset;
	Vector2D m_size;


};


#endif