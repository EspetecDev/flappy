#include "Game.h"

//Debug
#include <Windows.h>

float SCALING = 1.0f;
int SCREEN_HEIGHT = BASE_SCREEN_HEIGHT;
int SCREEN_WIDTH = BASE_SCREEN_WIDTH;

int main(int argc, char *argv[]) {

	// Debug
	AllocConsole();
	freopen("CON", "w", stdout);	

	Uint32 frameStart, frameTime;
	
	if (game::Instance()->init("Drone Hunt", 500, 100, false)) {
		while (game::Instance()->running()) {

			frameStart = SDL_GetTicks();

			game::Instance()->handleEvents();
			game::Instance()->update();
			game::Instance()->render();

			frameTime = SDL_GetTicks() - frameStart;

			if (frameTime < DELAY_TIME)
				SDL_Delay((int)(DELAY_TIME - frameTime));
		
		}
	}
	else {
		std::cout << "[main] Game init error" << std::endl;
		return -1;
	}
	

	game::Instance()->clean();

	return 0;
}
