#include "WaveSpawner.h"
#include <SDL.h>
#include "LoaderParams.h"
#include "Enemy.h"
#include "Game.h"



WaveSpawner::WaveSpawner(float enemyMultiplyer, int firstWave)
{
	m_waves.push_back(firstWave);
	m_waves.push_back((int)(firstWave*enemyMultiplyer));
	m_position.setX(0.0);
	m_position.setY(-50.0);
	m_enemyMultiplyer = enemyMultiplyer;
}

void WaveSpawner::spawnWaves() {
	if (m_currentWave < m_waves.size() && SDL_GetTicks() > m_nextWaveTime) {
		m_currentTime = SDL_GetTicks() / 1000;
		if (m_currentTime > m_lastTime + m_spawnRate) {
			m_lastTime = m_currentTime;
			std::cout << "[WaveSpawner::spawnWaves] Wave: " << m_currentWave << "  currentEnemies: " << m_waves[m_currentWave] << std::endl;
			createEvent();
			m_waves[m_currentWave]--;
			if (m_waves[m_currentWave] == 0) {
 				m_nextWaveTime = SDL_GetTicks() + 5000;
				m_waves.push_back((int)(m_waves.back() * m_enemyMultiplyer));
				m_currentWave++;
			}
		}
	}

	int nextPos = (int)getPosition().getX() + 10;
	// 70 : enemy width
	if (nextPos > (SCREEN_WIDTH - 70))
		nextPos = 0;
	setPosition(Vector2D((float)nextPos, getPosition().getY()));

	
}

void WaveSpawner::createEvent() {
	// Create Enemy
	SDL_Event event;
	SDL_memset(&event, 0, sizeof(event));
	event.type = createEntityEvent;
	event.user.data1 = new Enemy((new LoaderParams((int)m_position.getX(), (int)m_position.getY(), 70, 80, "enemy-still")),
		new Vector2D(0, 5.0f), new Vector2D(0, -0.02f), (SCREEN_HEIGHT/2)+(rand()%80));
	SDL_PushEvent(&event);
	SDL_free(&event.type);
	SDL_free(&event.user.data1);
	SDL_free(&event);
}




