#ifndef COLLISIONMANAGER_H
#define COLLISIONMANAGER_H

#include <vector>
#include "Collider.h"
class CollisionManager {
public:
		static CollisionManager* instance();
		void draw();
		void update();
		void clean();
		void addCollider(Collider* coll);
		void removeCollider(Collider* coll);

private:
		std::vector<Collider*> m_colliders;
		CollisionManager();
		CollisionManager(CollisionManager const&) {};             // copy constructor is private
		CollisionManager& operator=(CollisionManager const&) {};  // assignment operator is private
		static CollisionManager* m_pInstance;

		bool renderColliders = false;	//true para dibujar los colliders
};
#endif