#include "SDLGameObject.h"
#include "Game.h"
#include "CollisionManager.h"

SDLGameObject::SDLGameObject(const LoaderParams* pParams)
	: GameObject(pParams), m_position((float)pParams->getX(), (float)pParams->getY()), m_velocity(0.0f, 0.0f), m_acceleration(0.0f, 0.0f) {

	m_width = pParams->getWidth();
	m_height = pParams->getHeight();
	m_textureID = pParams->getTextureID();
	//m_collider = (new Collider(this));
	m_currentRow = 1;
	m_currentFrame = 0;
}

void SDLGameObject::draw() {

	TextureManager::Instance()->drawFrame(m_textureID, (int) m_position.getX(), (int)m_position.getY(),
		m_width, m_height, m_currentRow, m_currentFrame, game::Instance()->getRenderer());
	//(*m_collider).draw(game::Instance()->getRenderer());
}

void SDLGameObject::update(){
	m_velocity += m_acceleration;
	m_position += m_velocity;
}

void SDLGameObject::clean() {
	if ((m_collider) != 0) {
		//CollisionManager::instance()->removeCollider(m_collider);
		delete m_collider;
		m_collider = 0;
	}
	if (m_textureID != "") {
		m_textureID.clear();
	}
}

void SDLGameObject::collidedWith(SDLGameObject* other)
{

}
