#include "Enemy.h"
#include "Bullet.h"
Enemy::Enemy(const LoaderParams* pParams, Vector2D* velocity, Vector2D* acceleration, int limit) : SDLGameObject(pParams), m_limit(limit) {
	m_velocity.setX(velocity->getX());
	m_velocity.setY(velocity->getY());
	m_acceleration.setX(acceleration->getX());
	m_acceleration.setY(acceleration->getY());
	m_collider = (new Collider(this, new Vector2D(20,13), new Vector2D(35, 30)));
}

void Enemy::draw(){
	//std::cout << "[Enemy::draw] Drawing enemy " << std::endl;
	SDLGameObject::draw();	
}

void Enemy::update(){
	if (m_hp <= 0 && !m_dying)
		destroy();

	if (!m_dying) // If it's alive
	{
		// Move enemy
		if (m_position.getY() != m_limit) // If we haven't hit the limit
		{
			m_velocity += m_acceleration;
			if (m_velocity.getY() > 0.0f)
				m_position += m_velocity;

			if (m_position.getY() >= m_limit) //If we hit the limit
				m_position.setY((float)m_limit);
		}

		// Shoot
		m_coolDown--;
		Enemy::shoot();

		// Play animations
		if (m_textureID == "enemy-shooting" && m_currentFrame < ANIMATION_ENEMY_SHOOTING_FRAMES - 1)
		{
			if (SDL_GetTicks() - m_animationTimer > 50) {
				m_currentFrame++;
				m_animationTimer = SDL_GetTicks();
			}
		}
		else
		{
			m_textureID = "enemy-still";
			m_currentFrame = int((SDL_GetTicks() / 100) % 1);
		}
	}
	else // If it's dying
	{
		// Play animation
		if (SDL_GetTicks() - m_animationTimer > 50) {
			m_currentFrame++;
			m_animationTimer = SDL_GetTicks();
		}

		// Check if animation has been played entirely
		if (m_currentFrame > ANIMATION_ENEMY_SHOOTING_FRAMES - 1)
		{
			//Destroy enemy
			SDL_Event event;
			SDL_memset(&event, 0, sizeof(event));
			event.type = destroyEntityEvent;
			event.user.data1 = this;
			SDL_PushEvent(&event);
			event.type = NULL;
			event.user.data1 = nullptr;
		}
	}
}

void Enemy::hit(int damage)
{
	m_hp -= damage;
}

void Enemy::clean(){
	//std::cout << "[Enemy::clean] Cleaning enemy " << std::endl;
	SDLGameObject::clean();
	
}

void Enemy::shoot() {
	if (m_coolDown < 0) {
		m_coolDown = ENEMY_FRAMES_BETWEEN_BULLETS;

		// Play Animation

		m_textureID = "enemy-shooting";
		m_currentFrame = 0;
		m_animationTimer = SDL_GetTicks();

		//Play sfx
		AudioManager::Instance()->playSound("droneShot", 0);

		// Create Bullet
		Vector2D *playerPosition = ((Player*)player)->getCentre();
		Vector2D *enemyPosition = this->getCentre(); // More precision if given cannon position
		Vector2D *direction = new Vector2D(playerPosition->getX() - enemyPosition->getX(), playerPosition->getY() - enemyPosition->getY());
		direction->normalize();

		SDL_Event event;
		SDL_memset(&event, 0, sizeof(event));
		event.type = createEntityEvent;
		event.user.data1 = new Bullet(new LoaderParams((int)(m_position.getX() + 27), (int)(m_position.getY() + 60), 8, 8, "enemy-bullet"),
			&(direction->operator*(4.0f)), &(direction->operator*(0.0f)), false);
		SDL_PushEvent(&event);
		event.type = NULL;
		event.user.data1 = nullptr;

		delete playerPosition;
		delete enemyPosition;
		delete direction;
	}
}

void Enemy::destroy()
{
	//Change state
	m_dying = true;

	//Play animation
	m_textureID = "enemy-dying";
	m_currentFrame = 0;
	m_animationTimer = SDL_GetTicks();

	//Play sfx
	AudioManager::Instance()->playSound("droneDeath", 0);
	
	//Remove collider
	delete m_collider;
	m_collider = nullptr;

	//Update score
	SDL_Event event;
	SDL_memset(&event, 0, sizeof(event));
	event.type = updateScoreEvent;
	event.user.data1 = (void*)100;
	SDL_PushEvent(&event);
	event.type = NULL;
	event.user.data1 = nullptr;
}
