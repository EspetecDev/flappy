
#ifndef CONSTANTS_H
#define CONSTANTS_H

const int BASE_SCREEN_HEIGHT = 640;
const int BASE_SCREEN_WIDTH = 360;
extern float SCALING ;
extern int SCREEN_HEIGHT;
extern int SCREEN_WIDTH ;

const int FPS = 60;
const int DELAY_TIME = (int)(1000.0f / FPS);

typedef std::pair<std::string, SDL_Colour> mapPair_t;
const mapPair_t mapStartValues[] = {
	mapPair_t("white", {255, 255, 255, 255}),
	mapPair_t("black", {0, 0, 0, 255}),
	mapPair_t("red", {255, 0, 0, 255}),
	mapPair_t("green", {0, 255, 0, 255}),
	mapPair_t("blue", {0, 0, 255, 255}),
	mapPair_t("yellow", {255, 255, 0 ,255}),
	mapPair_t("aqua", {0 , 255, 255, 255}),
	mapPair_t("fuchsia", {255, 0, 255, 255})
};
const int mapStartValuesSize = sizeof(mapStartValues) / sizeof(mapStartValues[0]);
const std::map<std::string, SDL_Colour> TEXT_COLORS(mapStartValues, mapStartValues + mapStartValuesSize);

extern void* player;
const int PLAYER_FRAMES_BETWEEN_BULLETS = 10;
const int ENEMY_FRAMES_BETWEEN_BULLETS = 90;
const int ENEMY_INITIAL_COOLDOWN = 75;

extern Uint32 createEntityEvent;
extern Uint32 destroyEntityEvent;
extern Uint32 updateScoreEvent;
extern Uint32 deadStateEvent;

const int ANIMATION_PLAYER_SHOOTING_FRAMES = 8;
const int ANIMATION_PLAYER_DYING_FRAMES = 9;
const int ANIMATION_ENEMY_SHOOTING_FRAMES = 8;
const int ANIMATION_ENEMY_DYING_FRAMES = 8;
const int ANIMATION_ENEMY_DEAD_FRAMES = 15;

#endif