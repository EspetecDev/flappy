#ifndef INPUTHANDLER_H
#define INPUTHANDLER_H
#include "SDL.h"
#include <vector>
#include "Vector2D.h"
class InputHandler {

public:

	
	void initJoysticks();
	bool availableJoysticks() { return m_bAvailableJoysticks; }
	void update();
	void clean();
	float xvalue(int joy, int stick);
	float yvalue(int joy, int stick);
	bool getButtonState(int joy, int buttonNumber) { return m_buttonStates[joy][buttonNumber]; }
	bool getMouseButtonState(int buttonNumber) { return m_mouseButtonState[buttonNumber]; }
	Vector2D* getMousePosition() const { return m_mousePosition; }
	bool isKeyDown(SDL_Scancode key) const;
	static InputHandler* Instance() {
		if (s_pInstance == 0) {
			s_pInstance = new InputHandler();
			return s_pInstance;
		}
		return s_pInstance;
	}

private:
	// After : pointers are initialized with the value between ()
	InputHandler() : m_keyStates(0), m_bAvailableJoysticks(false), m_mousePosition(new Vector2D(0,0)){
		for (int i = 0; i < 3; i++)
			m_mouseButtonState.push_back(false);
	}
	~InputHandler(){}

	// Handle keyboard events
	void onKeyDown();
	void onKeyUp();

	// Handle mouse events
	void onMouseMove(SDL_Event& event);
	void onMouseButtonDown(SDL_Event& event);
	void onMouseButtonUp(SDL_Event& event);

	// Handle joystick events
	void onJoystickAxisMove(SDL_Event& event);
	void onJoystickButtonDown(SDL_Event& event);
	void onJoystickButtonUp(SDL_Event& event);
	
	// InputHandler instance
	static InputHandler* s_pInstance;
	// Gamepad joystic deadzone value
	const int m_joystickDeadZone = 10000;
	// Joysticks detected 
	std::vector<SDL_Joystick*> m_joysticks;
	// For every joystick, stick values
	std::vector<std::pair<Vector2D*, Vector2D*>> m_joystickValues;
	// Tells if joysticks could init correctly
	bool m_bAvailableJoysticks;
	// Button state array for every controller
	std::vector<std::vector<bool>> m_buttonStates;
	// Mouse button state array
	std::vector<bool> m_mouseButtonState;
	// Mouse position
	Vector2D* m_mousePosition;
	// Array pointer where keyboard keys will be stored
	const Uint8 *m_keyStates;
};
typedef InputHandler inputHandler;

enum mouseButtons {
	LEFT = 0,
	MIDDLE = 1,
	RIGHT = 2
};
#endif // !INPUTHANDLER_H

