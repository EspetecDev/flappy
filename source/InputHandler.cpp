#include "InputHandler.h"
#include <iostream>
#include "Game.h"

InputHandler* InputHandler::s_pInstance = 0;

void InputHandler::initJoysticks() {
	// Check if SDL_JOYSTICK was initialized
	if (SDL_WasInit(SDL_INIT_JOYSTICK) == 0)
		SDL_InitSubSystem(SDL_INIT_JOYSTICK);

	if (SDL_NumJoysticks() > 0) {
		for (int i = 0; i < SDL_NumJoysticks(); i++) {
			SDL_Joystick* joy = SDL_JoystickOpen(i);
			if (joy){
				// Adds every controller to vector
				m_joysticks.push_back(joy);
				// Inits the values of the sticks for every controller
				m_joystickValues.push_back(std::make_pair(new Vector2D(0, 0), new Vector2D(0, 0)));
				// Init controllers button states
				std::vector<bool> tempButtons;
				for (int j = 0; j < SDL_JoystickNumButtons(joy); j++) 
					tempButtons.push_back(false);
				m_buttonStates.push_back(tempButtons);
			}
			else
				std::cout << "[InputHandler::initJoystics] Coul not open joystick " << i << "." << std::endl;
		}
		SDL_JoystickEventState(SDL_ENABLE);
		m_bAvailableJoysticks = true;

		std::cout << "[InputHandler::initJoystics] Joysticks initialized: " << m_joysticks.size() << std::endl;
	}
	else
		m_bAvailableJoysticks = false;

}

void InputHandler::update() {
	SDL_Event event;
	
	while (SDL_PollEvent(&event))
		switch (event.type) {
		case SDL_QUIT:
			game::Instance()->quit();
			break;
		case SDL_JOYAXISMOTION:
			onJoystickAxisMove(event);
			break;
		case SDL_JOYBUTTONDOWN:
			onJoystickButtonDown(event);
			break;
		case SDL_JOYBUTTONUP:
			onJoystickButtonUp(event);
			break;
		case SDL_MOUSEMOTION:
			onMouseMove(event);
			break;
		case SDL_MOUSEBUTTONDOWN:
			onMouseButtonDown(event);
			break;
		case SDL_MOUSEBUTTONUP:
			onMouseButtonUp(event);
			break;
		case SDL_KEYDOWN:
			onKeyDown();
			break;
		case SDL_KEYUP:
			onKeyUp();
			break;
		default:
			break;
		}
}

void InputHandler::onKeyDown(){
	// Retrieve keyboard keys states
	m_keyStates = SDL_GetKeyboardState(0);
}

void InputHandler::onKeyUp(){
	// Retrieve keyboard keys states
	m_keyStates = SDL_GetKeyboardState(0);
}

void InputHandler::onMouseMove(SDL_Event & event){
	m_mousePosition->setX((float) event.motion.x);
	m_mousePosition->setY((float) event.motion.y);
}

void InputHandler::onMouseButtonDown(SDL_Event & event){
	if (event.button.button == SDL_BUTTON_LEFT)
		m_mouseButtonState[LEFT] = true;
	if (event.button.button == SDL_BUTTON_MIDDLE)
		m_mouseButtonState[MIDDLE] = true;
	if (event.button.button == SDL_BUTTON_RIGHT)
		m_mouseButtonState[RIGHT] = true;
}

void InputHandler::onMouseButtonUp(SDL_Event & event){
	if (event.button.button == SDL_BUTTON_LEFT)
		m_mouseButtonState[LEFT] = false;
	if (event.button.button == SDL_BUTTON_MIDDLE)
		m_mouseButtonState[MIDDLE] = false;
	if (event.button.button == SDL_BUTTON_RIGHT)
		m_mouseButtonState[RIGHT] = false;
}

void InputHandler::onJoystickAxisMove(SDL_Event& event) {
	// Get which controller is the input coming from
	// ONLY if we will use more than one controller
	int whichOne = event.jaxis.which;

	/* LEFT STICK */
	// Left stick moves left or right
	if (event.jaxis.axis == 0) {
		if (event.jaxis.value > m_joystickDeadZone)
			m_joystickValues[whichOne].first->setX(1);
		else if (event.jaxis.value < -m_joystickDeadZone)
			m_joystickValues[whichOne].first->setX(-1);
		else
			m_joystickValues[whichOne].first->setX(0);
	}
	// Left stick moves up or down
	if (event.jaxis.axis == 1) {
		if (event.jaxis.value > m_joystickDeadZone)
			m_joystickValues[whichOne].first->setY(1);
		else if (event.jaxis.value < -m_joystickDeadZone)
			m_joystickValues[whichOne].first->setY(-1);
		else
			m_joystickValues[whichOne].first->setY(0);
	}

	/* RIGHT STICK */
	// Right stick moves left or right
	if (event.jaxis.axis == 3) {
		if (event.jaxis.value > m_joystickDeadZone)
			m_joystickValues[whichOne].first->setX(1);
		else if (event.jaxis.value < -m_joystickDeadZone)
			m_joystickValues[whichOne].first->setX(-1);
		else
			m_joystickValues[whichOne].first->setX(0);
	}
	// Right stick moves up or down
	if (event.jaxis.axis == 4) {
		if (event.jaxis.value > m_joystickDeadZone)
			m_joystickValues[whichOne].first->setY(1);
		else if (event.jaxis.value < -m_joystickDeadZone)
			m_joystickValues[whichOne].first->setY(-1);
		else
			m_joystickValues[whichOne].first->setY(0);
	}
}

void InputHandler::onJoystickButtonDown(SDL_Event & event){
	// Activates the controller button read
	int whichOne = event.jaxis.which;
	m_buttonStates[whichOne][event.jbutton.button] = true;
}

void InputHandler::onJoystickButtonUp(SDL_Event & event){
	int whichOne = event.jaxis.which;
	m_buttonStates[whichOne][event.jbutton.button] = false;
}

void InputHandler::clean() {
	if (m_bAvailableJoysticks)
		for (int i = 0; i < SDL_NumJoysticks(); i++)
			SDL_JoystickClose(m_joysticks[i]);
}

// xvalue and yvalue will return (for the corresponding joy)
// the values of the stick where is pointing
// stick : 1 for left stick, 2 for right stick
float InputHandler::xvalue(int joy, int stick){
	if (m_joystickValues.size() > 0)
		if (stick == 1)
			return m_joystickValues[joy].first->getX();
		else if (stick == 2)
			return m_joystickValues[joy].second->getX();
	return 0.0f;
}

float InputHandler::yvalue(int joy, int stick){
	if (m_joystickValues.size() > 0)
		if (stick == 1)
			return m_joystickValues[joy].first->getY();
		else if (stick == 2)
			return m_joystickValues[joy].second->getY();
	return 0.0f;
}
// This function checks wether a key is pressed and
// updates our vector
bool InputHandler::isKeyDown(SDL_Scancode key) const{
	if (m_keyStates != 0) {
		if (m_keyStates[key] == 1)
			return true;
		else
			return false;
	}
	return false;
}
