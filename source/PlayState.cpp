#include "MainMenuState.h"
#include "TextureManager.h"
#include "Game.h"
#include "MenuButton.h"
#include "PlayState.h"
#include "DeadState.h"
#include "UIText.h"
#include "CollisionManager.h"
#include "AudioManager.h"
#include "InputHandler.h"
#include "PauseState.h"

const std::string PlayState::s_playStateID = "PLAY_STATE";
void* player;
TTF_Font* font,*font2;

void PlayState::update(){
	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_ESCAPE))
		game::Instance()->getStateMachine()->pushState(new PauseState());
	// debug
	//else if(InputHandler::Instance()->isKeyDown(SDL_SCANCODE_K))
	//	game::Instance()->getStateMachine()->changeState(new DeadState());
	else if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_K))
		AudioManager::Instance()->playMusic("gameLoop", -1);

	if (!m_gameObjects.empty()) {
		for (GameObject* go : m_gameObjects)
			go->update();
	}
	if (!m_uiTextVector.empty()) {
		for (UIText* ui : m_uiTextVector)
			ui->update();
	}
	CollisionManager::instance()->update();
	
	if (!AudioManager::Instance()->isMusicPlaying())
		AudioManager::Instance()->playMusic("gameLoop", -1);

	// Spawn waves
	m_waveSpawner->spawnWaves();
	m_lastWave = m_waveSpawner->getCurrentWave() + 1;

	PlayState::handleEvents();
}

void PlayState::handleEvents() {
	SDL_Event e;
	while (SDL_PollEvent(&e))
	{
		if (e.type == createEntityEvent)
		{
			m_gameObjects.push_back((GameObject*)e.user.data1);
		}
		else if (e.type == destroyEntityEvent)
		{
			for (unsigned int i = 0; i < m_gameObjects.size(); i++)
				if (m_gameObjects[i] == e.user.data1)
				{
					m_gameObjects[i]->clean();
					delete m_gameObjects[i];
					m_gameObjects.erase(m_gameObjects.begin() + i);
					m_gameObjects.shrink_to_fit();
					break;
				}
		}
		else if (e.type == updateScoreEvent)
		{
			m_score += (int)((unsigned long)e.user.data1 + (m_lastWave * 1.4));
			m_uiTextVector[1]->updateValues(" " + std::to_string(m_score), nullptr, nullptr, nullptr);
			game::Instance()->getStateMachine()->setCurrentScore(m_score);
		}
		else if (e.type == deadStateEvent)
		{
			game::Instance()->getStateMachine()->changeState(new DeadState());
		}
		// If it isn't one of these 2 events, it may be related to the mouse or to the windows and shoud be processed earlier (inputHandler)
		e.type = NULL;
		e.user.data1 = nullptr;
		e.user.data2 = nullptr;
	}
}

void PlayState::render(){
	if (!m_gameObjects.empty()) {
		for (GameObject* go : m_gameObjects)
			go->draw();
	}

	if (!m_uiTextVector.empty()) {
		m_uiTextVector[0]->updateValues("x " + std::to_string(((Player*)player)->getLives()), nullptr, nullptr, nullptr);
		for (UIText* ui : m_uiTextVector)
			ui->draw();
	}

	CollisionManager::instance()->draw();
}

bool PlayState::onEnter(){
	std::cout << "[PlayState::onEnter] Entering InfiniteModeState " << std::endl;

	// Load ENEMY textures
	if (!textureManager::Instance()->load("assets/enemy-still.png",    "enemy-still",    game::Instance()->getRenderer()))
		return false;
	if (!textureManager::Instance()->load("assets/enemy-shooting.png", "enemy-shooting", game::Instance()->getRenderer()))
		return false;
	if (!textureManager::Instance()->load("assets/enemy-bullet.png",   "enemy-bullet",   game::Instance()->getRenderer()))
		return false;
	if (!textureManager::Instance()->load("assets/enemy-dying.png",    "enemy-dying",    game::Instance()->getRenderer()))
		return false;

	// Load PLAYER textures
	if (!textureManager::Instance()->load("assets/player-still.png",    "player-still",    game::Instance()->getRenderer()))
		return false;
	if (!textureManager::Instance()->load("assets/player-shooting.png", "player-shooting", game::Instance()->getRenderer()))
		return false;
	if (!textureManager::Instance()->load("assets/player-bullet.png",   "player-bullet",   game::Instance()->getRenderer()))
		return false;
	if (!textureManager::Instance()->load("assets/player-dying.png",    "player-dying",    game::Instance()->getRenderer()))
		return false;
	if (!textureManager::Instance()->load("assets/player-dead.png",     "player-dead",     game::Instance()->getRenderer()))
		return false;

	// Load UI textures
	if (!textureManager::Instance()->load("assets/playerLivesIcon.png", "playerLivesIcon", game::Instance()->getRenderer()))
		return false;

	// Load Background
	if (!textureManager::Instance()->load("assets/background.png", "background", game::Instance()->getRenderer()))
		return false;

	GameObject* background = new SDLGameObject(new LoaderParams(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, "background"));
	m_gameObjects.push_back(background);

	GameObject* playerLivesIcon = new SDLGameObject(new LoaderParams(SCREEN_WIDTH / 3 * 0 + 30, -5, 50, 50, "playerLivesIcon"));
	m_gameObjects.push_back(playerLivesIcon);

	player = new Player(new LoaderParams(SCREEN_WIDTH / 2, SCREEN_HEIGHT - 100, 95, 95, "player-still"));
	m_gameObjects.push_back((GameObject*)player);

	//Init the TTF libray and declare two colors
	TTF_Init();
	
	//Open the font we want
	font = TTF_OpenFont("assets/Fonts/FFFFORWA.ttf", 13);
	font2 = TTF_OpenFont("assets/Fonts/FFFFORWA.ttf", 20);
	if(!font)
		std::cout << TTF_GetError() << std::endl;

	//Create the UITexts and push them into the list
	UIText* uiText1 = new UIText(new LoaderParams(SCREEN_WIDTH / 3 * 0 + 85, 15, 200, 200, "notUsed"), game::Instance()->getRenderer());
	UIText* uiText3 = new UIText(new LoaderParams(SCREEN_WIDTH / 3 * 2 + 25, 15, 200, 200, "notUsed"), game::Instance()->getRenderer());
	uiText1->load("x 3", TEXT_COLORS.find("black")->second, font, NULL);
	uiText3->load(" 0", TEXT_COLORS.find("black")->second, font2, NULL);

	m_uiTextVector.push_back(uiText1);
	m_uiTextVector.push_back(uiText3);

	// Create a WaveSpawner
	m_waveSpawner = new WaveSpawner(1.5, 3);
	m_lastWave = m_waveSpawner->getCurrentWave() + 1;
	m_nextWaveTime = SDL_GetTicks() + 15000;

	// Load music and sfx files
	audioManager::Instance()->load("assets/music/game-init.mp3", "gameInit", MUSIC);
	audioManager::Instance()->load("assets/music/game-loop.mp3", "gameLoop", MUSIC);
	audioManager::Instance()->load("assets/sfx/tank-shoot.wav",    "tankShot", SFX);
	audioManager::Instance()->load("assets/sfx/tank-death.wav",    "tankDeath", SFX);
	audioManager::Instance()->load("assets/sfx/drone-death.wav",   "droneDeath", SFX);
	audioManager::Instance()->load("assets/sfx/drone-shoot.wav",   "droneShot", SFX);

	audioManager::Instance()->playMusic("gameInit", 0);
	audioManager::Instance()->setMusicVolume(30);
	audioManager::Instance()->setSfxVolume("tankShot", 10);
	audioManager::Instance()->setSfxVolume("tankDeath", 80);
	audioManager::Instance()->setSfxVolume("droneDeath", 45);
	audioManager::Instance()->setSfxVolume("droneShot", 20);

	game::Instance()->getStateMachine()->setCurrentScore(m_score);

	return true;
}

bool PlayState::onExit(){
	std::cout << "[PlayState::onExit] Exiting MainMenuState " << std::endl;
	//Unload GameObjects
	for (GameObject* go : m_gameObjects) {
		go->clean();
		delete go;
	}
	m_gameObjects.clear();

	for (UIText* ui : m_uiTextVector) {
		ui->clean();
		delete ui;
	}
	m_uiTextVector.clear();
	
	//Unload textures
	textureManager::Instance()->clearFromTextureMap("enemy-still");
	textureManager::Instance()->clearFromTextureMap("enemy-shooting");
	textureManager::Instance()->clearFromTextureMap("enemy-bullet");
	textureManager::Instance()->clearFromTextureMap("enemy-dying");

	textureManager::Instance()->clearFromTextureMap("player-still");
	textureManager::Instance()->clearFromTextureMap("player-shooting");
	textureManager::Instance()->clearFromTextureMap("player-bullet");
	textureManager::Instance()->clearFromTextureMap("player-dying");
	textureManager::Instance()->clearFromTextureMap("player-dead");

	textureManager::Instance()->clearFromTextureMap("playerLivesIcon");

	textureManager::Instance()->clearFromTextureMap("background");

	//Unload sounds
	audioManager::Instance()->stopMusic(); // Halts the music 
	audioManager::Instance()->clearFromAudioMap("gameInit", MUSIC);
	audioManager::Instance()->clearFromAudioMap("gameLoop", MUSIC);
	audioManager::Instance()->clearFromAudioMap("tankShot", SFX);
	audioManager::Instance()->clearFromAudioMap("droneDeath", SFX);
	audioManager::Instance()->clearFromAudioMap("droneShot", SFX);
	
	TTF_CloseFont(font);
	TTF_Quit();

	return true;
}

void PlayState::s_deadState() {
	std::cout << "[PlayState::s_deadState] Dead!! " << std::endl;
	game::Instance()->getStateMachine()->changeState(new DeadState());
}