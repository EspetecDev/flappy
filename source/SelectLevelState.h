#ifndef SELECTLEVELSTATE_H
#define SELECTLEVELSTATE_H
#include "GameObject.h"
#include "GameState.h"
#include <iostream>
#include <vector>

class SelectLevelState : public GameState {

public:

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_selectLevelID; }

private:

	static const std::string s_selectLevelID;
	std::vector<GameObject*> m_gameObjects;

	// Callback functions for menu items
	static void s_toInfiniteMode();
	static void s_placeholder();
	
};

#endif