#include "CollisionManager.h"
#include <iostream>
#include "SDLGameObject.h"

CollisionManager* CollisionManager::m_pInstance = NULL;

CollisionManager * CollisionManager::instance()
{
	if (!m_pInstance)
		m_pInstance = new CollisionManager();
	return m_pInstance;
}

void CollisionManager::draw()
{
	if (!renderColliders) return;
	for (Collider* coll : m_colliders)
		coll->draw();
}

void CollisionManager::addCollider(Collider* coll) {
	m_colliders.push_back(coll);
}


void CollisionManager::removeCollider(Collider* coll) {
	std::vector<Collider*>::iterator position = std::find( m_colliders.begin(), m_colliders.end(), coll);
	if (position !=  m_colliders.end()) {
		m_colliders.erase(position);
		m_colliders.shrink_to_fit();
	}
}
void CollisionManager::update()
{
	if (m_colliders.size() == 0) return;
	for (unsigned int i = 0; i < m_colliders.size()-1; i++) {
		for (unsigned int j = i+1; j < m_colliders.size(); j++) {
			//if there is collision
			if (m_colliders[i]->checkIntersection(m_colliders[j]))
			{
				//Colision detectada, notificar los gameObjects.
				m_colliders[i]->getSDLGameObject()->collidedWith(m_colliders[j]->getSDLGameObject());
				m_colliders[j]->getSDLGameObject()->collidedWith(m_colliders[i]->getSDLGameObject());
			}
		}
	}
}

void CollisionManager::clean()
{
}

CollisionManager::CollisionManager() {

}



