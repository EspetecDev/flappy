
#include "SDLGameObject.h"
#include "CollisionManager.h"
#include "Game.h"
Collider::Collider(SDLGameObject* gameObject): m_positionOffset(0,0), m_size(0,0) {
	m_gameObject = gameObject;
	m_positionOffset.setX(0);
	m_positionOffset.setY(0);
	m_size.setX((float)m_gameObject->getWidth());
	m_size.setY((float)m_gameObject->getHeight());
	CollisionManager::instance()->addCollider(this);
}

Collider::Collider(SDLGameObject* gameObject, Vector2D* offset, Vector2D* size): m_positionOffset(0, 0), m_size(0, 0) {
	m_gameObject = gameObject;
	m_positionOffset = *offset;
	m_size = *size;
	CollisionManager::instance()->addCollider(this);
}

Vector2D  Collider::getPosition()
{ 
	return ( (m_gameObject->getPosition()) + m_positionOffset); 
}

void Collider::draw()
{
	SDL_Rect rect;
	rect.x = (int) getPosition().getX();
	rect.y = (int) getPosition().getY();
	rect.w = (int) m_size.getX();
	rect.h = (int) m_size.getY();
	SDL_SetRenderDrawColor(Game::Instance()->getRenderer(), 0, 255, 0, 255);
	
	SDL_RenderDrawRect(Game::Instance()->getRenderer(), &rect);
	SDL_SetRenderDrawColor(Game::Instance()->getRenderer(), 255, 0, 0, 255);
}

bool Collider::checkIntersection(Collider* other) {
	Vector2D pos1 = getPosition();
	Vector2D pos2 = other->getPosition();

	
	return (rangeIntersect(pos1.getX(), pos1.getX() + m_size.getX(), pos2.getX(), pos2.getX() + other->m_gameObject->getWidth()) &&
		rangeIntersect(pos1.getY(), pos1.getY() + m_size.getY(), pos2.getY(), pos2.getY() + other->m_gameObject->getHeight()));
}

bool Collider::rangeIntersect(float min1, float max1, float min2, float max2)
{
	if (
		fmax(min1, max1) >= fmin(min2, max2) &&
		fmin(min1, max1) <= fmax(min2, max2)
		) 
	{
		return true;
	}
	else 
	{
		/*std::cout << fmax(min1, max1) << ">=" << fmin(min2, max2) << std::endl 
			<< fmin(min1, max1) << "<=" << fmax(min2, max2) << std::endl;*/
		return false;
	}
}

Collider::~Collider() {
	CollisionManager::instance()->removeCollider(this);

}

