#include "Bullet.h"
#include "Enemy.h"

Bullet::Bullet(const LoaderParams* pParams, Vector2D* velocity, Vector2D* acceleration, bool ally) : SDLGameObject(pParams), m_ally(ally) {
	m_velocity.setX(velocity->getX());
	m_velocity.setY(velocity->getY());
	m_acceleration.setX(acceleration->getX());
	m_acceleration.setY(acceleration->getY());
	m_collider = new Collider(this);
}

void Bullet::draw(){
	//std::cout << "[Bullet::draw] Drawing bullet " << std::endl;
	SDLGameObject::draw();
}

void Bullet::update(){
	//Movement
	m_velocity += m_acceleration;
	m_position += m_velocity;

	if (m_position.getY() < -50 || m_position.getY() > SCREEN_HEIGHT + 50)
		destroy();

	//Animation
	m_currentFrame = int((SDL_GetTicks() / 100) % 2);
}

void Bullet::collidedWith(SDLGameObject* other) {

	if ((typeid(*other) == typeid(Enemy) ) && m_ally) {
		//std::cout << "[Bullet::collidedWith] did hit an enemy" << std::endl;
		((Enemy*)other)->hit(10);
		destroy();
	}

	if ((typeid(*other) == typeid(Player)) && !m_ally) {
		//std::cout << "[Bullet::collidedWith] did hit a player" << std::endl;
		((Player*)other)->hit(1);
		destroy();
	}
}

void Bullet::clean(){
	SDLGameObject::clean();
}

void Bullet::destroy()
{
	SDL_Event event;
	SDL_memset(&event, 0, sizeof(event));
	event.type = destroyEntityEvent;
	event.user.data1 = this;
	SDL_PushEvent(&event);
	event.type = NULL;
	event.user.data1 = nullptr;
}
