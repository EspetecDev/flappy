#include "MainMenuState.h"
#include "TextureManager.h"
#include "Game.h"
#include "MenuButton.h"
#include "PauseState.h"
#include "InputHandler.h"
#include "AudioManager.h"


const std::string PauseState::s_pauseID = "PAUSE";

void PauseState::s_pauseToMain()
{
	game::Instance()->getStateMachine()->popState();
	game::Instance()->getStateMachine()->changeState(new MainMenuState());
}

void PauseState::s_resumePlay()
{
	game::Instance()->getStateMachine()->popState();
}

void PauseState::update()
{
	if (!m_gameObjects.empty()) {
		for (unsigned int i = 0; i < m_gameObjects.size(); i++)
			m_gameObjects[i]->update();
	}
}

void PauseState::render()
{
	if (!m_gameObjects.empty()) {
		for (unsigned int i = 0; i < m_gameObjects.size(); i++)
			m_gameObjects[i]->draw();
	}
}

bool PauseState::onEnter()
{
	std::cout << "[PauseState::onEnter] Entering PauseState" << std::endl;
	unsigned long currentScore = game::Instance()->getStateMachine()->getCurrentScore();

	// Load buttons
	if (!TextureManager::Instance()->load("assets/resumeButton.png", "resumebutton", game::Instance()->getRenderer()))
		return false;

	if (!TextureManager::Instance()->load("assets/mainMenuButton.png", "mainbutton", game::Instance()->getRenderer()))
		return false;

	if (!textureManager::Instance()->load("assets/pauseBackground.png", "pausebackground", game::Instance()->getRenderer()))
		return false;

	GameObject* background = new SDLGameObject(new LoaderParams(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, "pausebackground"));
	m_gameObjects.push_back(background);

	GameObject* button1 = new MenuButton(new LoaderParams((SCREEN_WIDTH/2) - 81, (SCREEN_HEIGHT / 2)-39.5, 162, 75, "resumebutton"), s_resumePlay); //169, 379
	GameObject* button2 = new MenuButton(new LoaderParams((SCREEN_WIDTH / 2) - 65, (SCREEN_HEIGHT / 2) - 30 + 90, 130, 60, "mainbutton"), s_pauseToMain); // 205, 506
	m_gameObjects.push_back(button1);
	m_gameObjects.push_back(button2);

	// Load score text
	TTF_Font* font2;
	font2 = TTF_OpenFont("assets/Fonts/FFFFORWA.ttf", 20);
	if (!font2)
		std::cout << TTF_GetError() << std::endl;

	UIText* score = new UIText(new LoaderParams((SCREEN_WIDTH / 2) - 75, (SCREEN_HEIGHT / 4)+39, 110, 39, "score"), game::Instance()->getRenderer()); //163, 309
	score->load("Score: " + std::to_string(currentScore), TEXT_COLORS.find("black")->second, font2, NULL);
	m_gameObjects.push_back(score);

	TTF_CloseFont(font2);

	//audioManager::Instance()->setMusicVolume(audioManager::Instance()->get)

	return true;
}

bool PauseState::onExit()
{
	for (unsigned int i = 0; i < m_gameObjects.size(); i++) {
		m_gameObjects[i]->clean();
		delete m_gameObjects[i];
	}
	m_gameObjects.clear();
	m_gameObjects.shrink_to_fit();

	for (UIText* ui : m_uiTextVector) {
		ui->clean();
		delete ui;
	}
	m_uiTextVector.clear();
	m_uiTextVector.shrink_to_fit();

	TextureManager::Instance()->clearFromTextureMap("resumebutton");
	TextureManager::Instance()->clearFromTextureMap("mainbutton");
	TextureManager::Instance()->clearFromTextureMap("pausebackground");

	audioManager::Instance()->clearFromAudioMap("tankDeath", SFX);
	// reset the mouse button states to false
	//InputHandler::Instance()->reset();
	std::cout << "[PauseState::onEnter] Exiting PauseState" << std::endl;
	return true;
}