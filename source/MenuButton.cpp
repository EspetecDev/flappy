#include "MenuButton.h"
#include "InputHandler.h"

MenuButton::MenuButton(const LoaderParams* pParams, void (*callback)() ) : SDLGameObject(pParams), m_callback(callback) {
	// Start at frame 0
	m_currentFrame = MOUSE_OUT; 
	m_bRealeased = true;
}

void MenuButton::draw(){
	SDLGameObject::draw();
}

void MenuButton::update() {
	// Get actual mouse position and check if inside bounds
	Vector2D* pMousePos = inputHandler::Instance()->getMousePosition();
	if (pMousePos->getX() < (m_position.getX() + m_width)
		&& pMousePos->getX() > m_position.getX()
		&& pMousePos->getY() < (m_position.getY() + m_height)
		&& pMousePos->getY() > m_position.getY()) {

		//Mouse click down
		if (inputHandler::Instance()->getMouseButtonState(LEFT) && m_bRealeased) {
			m_currentFrame = CLICKED;
			m_bRealeased = false;
		}
		//Mouse click up, so we callback
		else if (!inputHandler::Instance()->getMouseButtonState(LEFT) && !m_bRealeased) {
			m_currentFrame = CLICKED;
			m_bRealeased = true;
			// Calling callback function
			m_callback();			
		}
		//Mouse just over it
		else if (!inputHandler::Instance()->getMouseButtonState(LEFT)) {
			m_currentFrame = MOUSE_OVER;
			m_bRealeased = true;
		}		
	}
	//Mouse outside
	else {
		m_currentFrame = MOUSE_OUT;
		m_bRealeased = true;
	}
		
}

void MenuButton::clean(){
	SDLGameObject::clean();
}

