#include "SelectLevelState.h"
#include "MainMenuState.h"
#include "TextureManager.h"
#include "Game.h"
#include "MenuButton.h"
#include "PlayState.h"
#include "DeadState.h"

const std::string SelectLevelState::s_selectLevelID = "SELECT_LEVEL";

void SelectLevelState::update(){
	for (GameObject* go : m_gameObjects)
		go->update();
}

void SelectLevelState::render(){
	for (GameObject* go : m_gameObjects)
		go->draw();
}

bool SelectLevelState::onEnter(){
	std::cout << "[SelectLevelState::onEnter] Entering SelectLevelState " << std::endl;

	if (!textureManager::Instance()->load("assets/infinite.png", "infiniteLevelButton", game::Instance()->getRenderer()))
		return false;
	if (!textureManager::Instance()->load("assets/placeholder.png", "phButton1", game::Instance()->getRenderer()))
		return false;
	if (!textureManager::Instance()->load("assets/placeholder.png", "phButton2", game::Instance()->getRenderer()))
		return false;

	if (!textureManager::Instance()->load("assets/background.png", "background", game::Instance()->getRenderer()))
		return false;

	GameObject* background = new SDLGameObject(new LoaderParams(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, "background"));
	m_gameObjects.push_back(background);

	GameObject* infiniteLevelButton = new MenuButton(new LoaderParams(100, 100, 400, 100, "infiniteLevelButton"), s_toInfiniteMode);
	GameObject* phButton1 = new MenuButton(new LoaderParams(100, 300, 400, 100, "phButton1"), s_placeholder);
	GameObject* phButton2 = new MenuButton(new LoaderParams(100, 500, 400, 100, "phButton2"), s_placeholder);

	m_gameObjects.push_back(infiniteLevelButton);
	m_gameObjects.push_back(phButton1);
	m_gameObjects.push_back(phButton2);

	return true;
}

bool SelectLevelState::onExit(){
	std::cout << "[SelectLevelState::onExit] Exiting SelectLevelState " << std::endl;
	for (GameObject* go : m_gameObjects)
		go->clean();
	m_gameObjects.clear();
	textureManager::Instance()->clearFromTextureMap("infiniteLevelButton");
	textureManager::Instance()->clearFromTextureMap("phButton1");
	textureManager::Instance()->clearFromTextureMap("phButton2");

	return true;
}

void SelectLevelState::s_toInfiniteMode(){
	std::cout << "Infinite Mode button clicked!" << std::endl;
	game::Instance()->getStateMachine()->changeState(new PlayState());
}

void SelectLevelState::s_placeholder() {
	std::cout << "Placeholder!" << std::endl;
}

